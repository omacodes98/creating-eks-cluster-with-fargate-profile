# Demo Project - EKS cluster with Fargate Profile 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Create Fargate IAM Role 

* Create Fargate Profile 

* Deploy an  application to EKS cluster using Fargate profile 

## Technologies Used 

* Kubernetes 

* AWS EKS 

* AWS Fargate 

## Steps 

Step 1: Create an IAM role for fargate profile 

[Creating role](/images/01_creating_role_for_fargate_selecting_trusted_entity.png)

Step 2: Select  EKS fargate pod as use case 

[Use case](/images/02_selecting_use_case.png)

Step 3: Select Amazon EKS Fargate pod execution role policy 

[Selected Policy](/images/03_choose_permission_policy.png)

Step 4: Give the role a good name 

[Naming Role](/images/04_give_the_role_a_name.png)

[Created Fargate Role](/images/05_created_fargate_role.png)

Step 5: Create a fargate profile and make sure you attach the role that was created earlier to the fargate profile 

[Creating Fargate Profile](/images/06_create_fargate_profile.png)

Step 6: Configure pod selector, this will help fargate know the right pod to control. You do this by giving it the name of the namespace you want it to control or by adding match labels to allow it know the pods it has control over.

[Configure pod selection](/images/07_fargate_configuration_pod_selector.png)

Step 7: Go to your deployment file and add namespaces or matchlabels for fargate access and control 

[Deployment file for fargate access](/images/08_adding_namespace_and_labels_to_deployment_file_to_enable_fargate_to_know_the_pods_to_schedule.png)

[Created fargate profile](/images/09_created_fargate_profile.png)

[extra info](/images/10_extra_info_fargate_and_node_group_in_compute.png)

Step 8: Create namespace to allow fargate know the right pods to have control of 

    kubectl create ns dev

[Created namespace](/images/11_creating_namespace_dev.png)

Step 9: Apply updated config file which contains deployment and service configuration

    kubectl apply -f nginx-config.yaml 

[Apply Config file](/images/12_applying_updated_nginx_config_file_with_the_namespace_and_labels.png)

Step 10: Check if pod was successful

    kubectl get pods -n dev 

[Pods running](/images/13_pod_running_in_dev_ns.png)

[Worker nodes](/images/14_worker_nodes.png)


## Usage 

    kubectl apply -f nginx-config.yaml


## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/creating-eks-cluster-with-fargate-profile.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/creating-eks-cluster-with-fargate-profile

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.